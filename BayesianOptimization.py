import pandas as pd
import numpy as np
import time

from hyperopt import fmin, tpe, Trials
from hyperopt import STATUS_OK
from hyperopt import space_eval
from hyperopt import hp


from sklearn.metrics import mean_squared_error, make_scorer

from sklearn.model_selection import cross_val_score, GridSearchCV, RandomizedSearchCV
from sklearn.model_selection import train_test_split

from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet

from sklearn.ensemble import RandomForestClassifier
from sklearn import datasets
from sklearn import pipeline


import lightgbm as lgb

from sklearn.ensemble import GradientBoostingRegressor

import xgboost as xgb

"""
    This code has the intention of helping hyperparameters optimization.
    There are a lot of téchniques for example: Grid search and Random search
    I chose to use Bayesian optimization, since this algorithm is quit fast in tuning the parameters.
"""

#  Specify algorithms
models = {
   'logistic_regression': LogisticRegression,
   'ridge': Ridge,
   'lasso': Lasso,
   'elasticnet': ElasticNet,
   'rf' : RandomForestClassifier, 
   'knn' : KNeighborsClassifier,
   'svc' : SVC,
   'lgb' : lgb,
   'lgb_regressor' : lgb.LGBMRegressor,
   'gb_regressor' : GradientBoostingRegressor,
   'xgb_regressor' : xgb.XGBRegressor,   
}

# Specify the hyperparameters
def search_space(model):
 
    model = model.lower()
    space = {}
 
    if model == 'knn': 
        space = {
            'n_neighbors': hp.choice('n_neighbors', range(1,100)),
            'algorithm': hp.choice('algorithm', ['ball_tree', 'kd_tree', 'brute']),
            'leaf_size': hp.choice('leaf_size', range(1,10)),
    }
 
    elif model == 'svc':
         space = {
             'C': hp.uniform('C', 0, 20),
             'kernel': hp.choice('kernel', ['linear', 'sigmoid', 'poly', 'rbf']),
             'gamma': hp.uniform('gamma', 0, 20), 
         }
 
    elif model == 'logistic_regression':
         space = {
             'warm_start' : hp.choice('warm_start', [True, False]),
             'fit_intercept' : hp.choice('fit_intercept', [True, False]),
             'tol' : hp.uniform('tol', 0.00001, 0.0001), 
             'C' : hp.uniform('C', 0.05, 3),
             'solver' : hp.choice('solver', ['newton-cg', 'lbfgs', 'liblinear']),
             'max_iter' : hp.choice('max_iter', range(100,1000)),
             'multi_class' : 'auto',
             'class_weight' : 'balanced'
      }
 
    elif model == "rf":
         space = {
             "max_depth": hp.choice("max_depth", range(1,20)), 
             "max_features": hp.choice("max_features", range(1,3)),
             "n_estimators": hp.choice("n_estimators", range(10,50)), 
             'criterion': hp.choice('criterion', ["gini", "entropy"]),
     }
     
    elif model == "ridge":
         space = {
             "alpha": hp.uniform("alpha", 10**-9, 10**-5),
             'solver': hp.choice('solver', ["auto", "svd", "cholesky", "lsqr"]),
    }
            
    elif model == "lasso":
         space = {
             "alpha": hp.uniform("alpha", 10**-9, 10**-5),
             'selection': hp.choice('selection', ["cyclic", "random"]),
     }
            
    elif model == "elasticnet":
         space = {
             "alpha": hp.uniform("alpha", 10**-9, 10**-5),
             "l1_ratio": hp.uniform("l1_ratio", 10**-2, 1),
             'selection': hp.choice('selection', ["cyclic", "random"]),
     }
    
#     elif model == "lgb":
#          space = {
#              "learning_rate": hp.uniform("learning_rate", 0.01, 0.09),
#              "boosting_type": hp.uniform("l1_ratio", 10**-2, 1),
#              'metric': hp.choice('selection', ["cyclic", "random"]),
#              'max_depth': hp.choice('max_depth', [2 , 5, 10, 20]),
#              'early_stopping_rounds': hp.choice('early_stopping_rounds', [3, 5]),
             
#      }

# https://lightgbm.readthedocs.io/en/latest/pythonapi/lightgbm.LGBMRegressor.html
    elif model == "lgb_regressor":
         space = {
             "learning_rate": hp.uniform("learning_rate", 0.01, 0.09),
             "boosting_type": hp.choice("boosting_type", ['gbdt']), #['gbdt', 'dart', 'goss', 'rf']
#             'max_depth': hp.choice('max_depth', [40 , 50, 70, 80, 90]),
#              'early_stopping_rounds': hp.choice('early_stopping_rounds', [3, 5, 10]),
#             'num_leaves ': hp.choice('num_leaves ', [40 , 50, 70, 80, 90]),
#             'n_estimators ': hp.choice('num_leaves', [100 , 200, 400, 500]),
             'reg_alpha': hp.uniform("reg_alpha", 0.01, 0.09),
             'reg_lambda': hp.uniform("reg_lambda", 0.01, 0.09),
             
     }             
    
    elif model == "gb_regressor":
         space = {
             "loss": hp.choice("loss", ['ls']), # ['ls','lad','huber','quantile']
             "learning_rate": hp.uniform("learning_rate", 0.01, 0.09),
             "n_estimators": hp.choice("n_estimators", [100, 200, 500, 700]),
             'max_depth': hp.choice('max_depth', [2 , 5, 10, 20]),
             "alpha": hp.uniform("alpha", 10**-9, 10**-5),
     }
    
    elif model == "xgb_regressor":
         space = {
             "min_child_weight": hp.choice("min_child_weight", [1, 5, 10]),
             "gamma": hp.uniform("gamma", 0.01, 0.09),
             "subsample": hp.choice("subsample", [1, 3, 5, 7]),
             "colsample_bytree": hp.uniform("colsample_bytree", 0, 1),
             'max_depth': hp.choice('max_depth', [2 , 5, 10, 20]),
             "learning_rate": hp.uniform("learning_rate", 0.01, 0.09),
             "reg_alpha": hp.uniform("reg_alpha", 0.01, 0.09),
             "reg_lambda": hp.uniform("reg_lambda", 0.01, 0.09),
     }

        
    return space

# Bayesian hyperparameters tuning
def search(pipeline, parameters, X_train, y_train, X_test, y_test, n_iter=None):
    
    start = time.time() 
    
    def objective_function(params):
        clf = pipeline(**params)
        
        # Define error: RMSE
        scorer = make_scorer(mean_squared_error, greater_is_better = False)
        rmse= np.sqrt(-cross_val_score(clf, X_train, y_train, scoring = scorer, cv = 5)).mean()
        
        return {'loss': rmse, 'status': STATUS_OK} 
    
    trials = Trials()
    best_param = fmin(objective_function, 
                      parameters, 
                      algo=tpe.suggest, 
                      max_evals=n_iter, 
                      trials=trials
                      ) #rstate= np.random.RandomState(1)
    
    loss = [x['result']['loss'] for x in trials.trials]
    
    best_param = space_eval(parameters, best_param)
    clf_best = pipeline(**best_param)                                  
    clf_best.fit(X_train, y_train)
    
    # Printing the results of the optimization
    print("")
    print("##### Results")
    print("Model: ", pipeline)
    print("Score best parameters: ", min(loss)*-1)
    print("Best parameters: ", best_param)
    print("Train Score: ", clf_best.score(X_train, y_train))
    print("Test Score: ", clf_best.score(X_test, y_test))
    print("Time elapsed: ", time.time() - start)
    print("Parameter combinations evaluated: ", n_iter)
    
    return best_param

